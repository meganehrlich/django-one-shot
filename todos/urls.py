from django.urls import path
from todos.views import (
    todo_lists, 
    details_of_list, 
    create_list,
    todo_list_update,
    todo_list_delete,
    todo_item_create,
    todo_item_update
)

urlpatterns = [
    path("", todo_lists, name="todo_list_list"),
    path("<int:pk>/", details_of_list, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:pk>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:pk>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:pk>/edit/", todo_item_update, name="todo_item_update")
]
