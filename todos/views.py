from django.shortcuts import get_object_or_404, render, redirect, HttpResponseRedirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_lists(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "todos/list.html", context)


def details_of_list(request, pk):
    tododetails = TodoList.objects.get(pk=pk)
    context = {
        "tododetails": tododetails
    }

    return render(request, "todos/details.html", context)


def create_list(request):
    context = {}
    form = TodoListForm(request.POST or None)
    if form.is_valid():
        formvar = form.save()
        return redirect('todo_list_detail', formvar.pk)

    context['form'] = form
    return render(request, "todos/create.html", context)


def todo_list_update(request, pk):
    context = {}
    object = get_object_or_404(TodoList, pk=pk)
    form = TodoListForm(request.POST or None, instance = object)

    if form.is_valid():
        form = form.save()
        return redirect('todo_list_detail', form.pk)

    context["form"] = form

    return render(request, "todos/update.html", context)


def todo_list_delete(request, pk):
    context = {}
    object = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        object.delete()

        return redirect('todo_list_list')
    return render(request, "todos/delete.html", context)

    
def todo_item_create(request):
    context = {}
    form = TodoItemForm(request.POST or None)
    if form.is_valid():
        anothervar = form.save()
        return redirect('todo_list_detail', anothervar.pk)

    context['form'] = form
    return render(request, 'todos/createitem.html', context)


def todo_item_update(request, pk):
    context = {}
    object = get_object_or_404(TodoItem, pk=pk)
    form = TodoItemForm(request.POST or None, instance=object)

    if form.is_valid():
        myvar = form.save()
        return redirect('todo_list_detail', myvar.pk)

    context["form"] = form
    return render(request, "todos/edititem.html", context)
