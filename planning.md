Set up
This is a Django application. You need to create a new virtual environment and install the dependencies for Django and software development.

If you don't remember the commands for this, check out the next page of today's exploration, which is a Reference Guide.

* [x] Fork and clone the starter project from django-one-shot 
* [x]Create a new virtual environment in the repository directory for the project
* [x]Activate the virtual environment
* [x]Upgrade pip
* [x]Install django
* [x]Install black
* [x]Install flake8
* [x]Install djlint
* [x]Deactivate your virtual environment
* [x]Activate your virtual environment
* [x]Use pip freeze to generate a requirements.txt file

In the repository directory:
Feature 2
* [x] in one-shot create my project: django-admin startproject brain_two .
* [x] in one-shot create todos app: manage.py startapp todos
* [x] install brain_two project in [] INSTALLED_APPS list todos.apps.TodosConfig
* [x] In terminal manage.py makemigrations then manage.py migrate
* [x] In terminal manage.py createsuperuser --> follow through

Feature 3
* [x] Create ToDoList model in models.py
* [x] return __str__(self): self.name
* [x] Make migrations
* [x] Test in django shell
* [x] In shell import ToDoList and use ToDoList.objects.create to create new TodoList.
* [x] git add .,  git commit -m"", git push

Feature 4
* [x] In Admin admin.site.register(ToDoList)
* [x] Check if working, add todo items through admin on browser

Feature 5
* [x] This feature is for you to create a TodoItem model in the todos Django app.
* [x] Moreover, the TodoItem model should implicitly convert to a string with the __str__ method that is the value of the task property.
* [x] Don't forget to make migrations and migrate your database.
* [x] To test this model, you can run the django shell, import the model, and create a few TodoItems.
* [x] It is good to add, commit, and push your progress every single time you finish a section of this project.

Feature 6
* [x] Register the TodoItem model with the admin so that you can see it in the Django admin site.
* [x] Test

Feature 7
* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.

    The resulting HTML from a request to the path http://localhost:8000/todos/  should result in HTML that has:

    * [x] the fundamental five in it
    * [x] a main tag that contains:
      * [x] div tag that contains:
        * [x] an h1 tag with the content "My Lists"
          * [x] a table that has two columns:
            * [x] the first with the header "Name" and the rows with the names of the Todo lists
            * [x]the second with the header "Number of items" and nothing in those rows because we don't yet have tasks
* [x] Test this by navigating to http://localhost:8000/todos.
* [x] It is good to add, commit, and push your progress every single time you finish a section of this project.

Feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [x] Create a template to show the details of the todolist and a table of its to-do items
* [x] Update the list template to show the number of to-do items for a to-do list
* [] Update the list template to have a link from the to-do list name to the detail view for that to-do list
  * [] the fundamental five in it
    * [] a main tag that contains:
      * [] div tag that contains:
        * [] an h1 tag with the to-do list's name as its content
        * [] an h2 tag that has the content "Tasks"
            * [] a table that contains two columns with the headers "Task" and "Due date" with rows for each task in the to-do list